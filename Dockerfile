FROM lambci/lambda:build-python3.6

# Replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN yum install -y curl build-essential libssl-dev

## Install SSH server for remote debugging if available on the IDE
#RUN yum install -y yum-plugin-ovl && yum clean all
#RUN yum install -y openssh-server
#
#RUN mkdir /var/run/sshd
#RUN echo 'root:testssh' | chpasswd
#RUN sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
#RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
#RUN sed -i 's/#PermitRootLogin /PermitRootLogin /' /etc/ssh/sshd_config
#RUN sed -i 's/#MaxStartups /MaxStartups /' /etc/ssh/sshd_config
#RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd
#RUN echo "export VISIBLE=now" >> /etc/profile

ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 9.8.0

# Install nvm with node and npm
RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.33.8/install.sh | bash \
    && source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

RUN mkdir /usr/app
RUN mkdir /usr/app/log
# CREATE DIRECTORIES FOR AWS CREDENTIALS AND APPLICATIONS
RUN mkdir /root/.aws
RUN mkdir /usr/app/external_app

WORKDIR /usr/app/external_app

# log dir
VOLUME /usr/app/log

# Bundle app source
#COPY . /usr/app
# Install app dependencies
RUN ln -s /usr/local/nvm/versions/node/v9.8.0/bin/npm /usr/bin/npm
#RUN command -v nvm
#RUN nvm ls
# INSTALL SERVERLESS
RUN npm install serverless -g
#RUN sls plugin install -n serverless-python-requirements

EXPOSE  3000
#CMD ["node", "server.js"]

# Fancy prompt to remind you are in serverless-lambda
RUN echo "alias ll='ls -alF'" >> /root/.bashrc
