# AWS Google Analytis Downloader

This repository contains the code that allows to perform ETL for the Google Analytics search keywords.

This project uses these AWS services: Step Functions, Lambda, Lambda layers, Secret Manager, Athena, SNS.

The state machine performs the download of the Google Analytics search keyword statistics inside the first Lambda (`GetSearchKeywordToIntakeTransition`), then saves the parquet with the second Lambda (`GetSearchKeywordToIntakeRaw`). The last Lambda (`GetSearchKeywordToAthenaRaw`) add a new partition to the Athena table.

## Requirements

- Layers: `googleapi_lib` and `pandas_lib`.

- Once they are deployed, check the ARN associated with Lambdas (see file ./serverless-parts/functions.yml) and replace "ARN-OF-THE-LAYER" accordingly.

- Create an Athena database called, for example, `google_analytics`.

- Set the AWS Secret Manager with the content of the Google Analytics service account JSON:

```json
{
    "type": "service_account",
    "project_id": "your-project-id",
    "private_key_id": "1a2b3c5d8e13f21g....ecc",
    "private_key": "-----BEGIN PRIVATE KEY-----\nA1B2C3D4.....ecc...\n-----END PRIVATE KEY-----\n",
    "client_email": "example@your-project-id.iam.gserviceaccount.com",
    "client_id": "yourcliend_id123581321",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/....."
}
```

## Change configuration files

Change the main configuration file inside the conf-env/ folder (there is a conf-dev.yml example file).

The file name is used in conjunction with other variables (i.e. region: ${file(./conf-env/conf-${self:provider.stage}.yml):region}) so pay attention to this name or change it accordingly.


Change in the googleapi_lib configuration file (./layers/googleapi_lib/serverless.yml) and in the pandas_lib configuration file (./layers/pandas_lib/serverless.yml) the following variables:

- name: YOUR_VAR_NAME
- stage: YOUR_VAR_STAGE
- region: YOUR_VAR_REGION
- deploymentBucket: YOUR_VAR_DEPLOYMENT_BUCKET

### Layers regeneration

If you prefer to regenerate a layer, you can:

```bash
$: virtualenv -p python3 venv
$: . venv/bin/activate
$: pip3 install -r requirements.txt -t layer-dir/python/lib/python3.6/site-packages/ --upgrade
$: cd layer-dir
$: zip -r file.zip .
```

Be sure that the structure is:

```
layer-dir/file.zip
serverless.yml
```

before deploying.


## Deploy and first run

Deploy the serverless application:

```bash
:$ serverless deploy --aws-profile YOURAWSPROFILENAME --stage YOURSTAGE
```

and do a first run so that file inside raw and transition will be created.

This run will fail but we need only new data to run a Glue Crawler for the first time and create the new table.


You can also make the table manually:

```sql
CREATE EXTERNAL TABLE `search_keyword`(
  `date` timestamp,
  `keyword` string,
  `uniques` bigint,
  `sessions` bigint,
  `depth` bigint,
  `refinements` bigint,
  `duration` bigint,
  `exits` bigint)
PARTITIONED BY (
  `dumpdate` string)
ROW FORMAT SERDE
  'org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe'
STORED AS INPUTFORMAT
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat'
OUTPUTFORMAT
  'org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat'
LOCATION
  's3://YOUR-BUCKET-NAME/intake/raw/google_analytics/search_keyword/'
```


Re-run the process which should be end up correctly.


## BONUS

You can manually run `./src/_manual__history_loader.py` to create the history of the data.

Please change `YOURAWSPROFILENAME` and `YOURAWSACCOUNTID` with your values and then run the script locally.


## Notes

You may want to change the downloaded metrics in `./src/get_search_keyword__to_intake_transition.py`:

```
GA_METRICS = [
    'ga:searchUniques', # Total Unique Searches
    'ga:searchSessions', # Sessions with Search
    'ga:searchDepth', # Search Depth
    'ga:searchRefinements', # Search Refinements
    'ga:searchDuration', # Time after Search
    'ga:searchExits', # Search Exits
]
```

If so, change also `GA_METRICS_CSV_HEADER` and the Athena table above.
Check also the `./src/get_search_keyword__to_intake_raw.py` contents (you may need to add some transformations).


# Authors

"AWS Google Analytis Downloader" is created by [Linkalab S.r.l.](http://www.linkalab.it)


# License

GNU General Public License v3.0 or later

See `LICENSE` to see the full text.