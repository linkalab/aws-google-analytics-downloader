#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os, io, json, traceback, sys, csv, base64
import boto3
from botocore.exceptions import ClientError
from datetime import datetime, timezone, timedelta

# Libs from Layer
from googleapiclient.errors import HttpError
from googleapiclient import sample_tools
from oauth2client.service_account import ServiceAccountCredentials
from httplib2 import Http
from apiclient.discovery import build

# Shared
from src.common.shared_functions import sf_get_logger, sf_resp_payload, sf_error_paylod

"""
@thanks to: https://www.compose.com/articles/loading-google-analytics-data-to-postgresql-using-python/
@see also:
- https://www.jcchouinard.com/get-all-keywords-with-search-console-api/
- https://developers.google.com/webmaster-tools/search-console-api-original/v3/quickstart/quickstart-python
"""

logger = sf_get_logger()

S3_BUCKET = os.getenv('s3_bucket', 'ERROR_s3_bucket')
S3_INTAKE_KEY = os.getenv('intake_transition_zone_s3_prefix', 'ERROR_intake_transition_zone_s3_prefix') # with trailing slash
SECRET_MANAGER_NAME = os.getenv('secret_manager_name', 'ERROR_secret_manager_name')
AWS_REGION = os.getenv('aws_region', 'ERROR_aws_region')
GA_PROJECT_ID = os.getenv('ga_project_id', 'ERROR_ga_project_id')

GA_METRICS = [
    'ga:searchUniques', # Total Unique Searches
    'ga:searchSessions', # Sessions with Search
    'ga:searchDepth', # Search Depth
    'ga:searchRefinements', # Search Refinements
    'ga:searchDuration', # Time after Search
    'ga:searchExits', # Search Exits
]
GA_METRICS_CSV_HEADER = [
    "date",
    "keyword",
    "uniques",
    "sessions",
    "depth",
    "refinements",
    "duration",
    "exits"
]

def handler(event, context):
    logger.info("#: Event: %s" % event)
    try:
        if "date" in event: # i.e. '2020-02-21'
            dl_day = event["date"]
            dumpdate_str = dl_day.replace('-', '')
        else:
            now = datetime.now().replace(hour=0, microsecond=0, second=0, minute=0) - timedelta(days=1)
            dl_day = "yesterday"
            dumpdate_str = datetime.strftime(now, '%Y%m%d')

        csv_name = f"ga_search_keyword_{dumpdate_str}.csv"
        s3_key = f"{S3_INTAKE_KEY}search_keyword/dumpdate={dumpdate_str}/{csv_name}"

        ga_secrets = get_secret()
        results = get_search_keyword(ga_secrets, dl_day)
        out_csv = create_csv(results, csv_name)

        s3_client = boto3.client('s3')
        s3_client.put_object(Bucket=S3_BUCKET, Key=s3_key, Body=open(out_csv, 'rb'))
        logger.info("#: Object stored in: %s" %s3_key)

        res = {
            "s3_key": s3_key,
            "dumpdate": dumpdate_str
        }

        return sf_resp_payload("IN_TRANS_OK", res)

    except Exception as e:
        error = traceback.format_exc()
        logger.error("Error: %s" %error, exc_info=True)
        raise e


def get_search_keyword(ga_secrets, dl_day):
    scopes = ['https://www.googleapis.com/auth/analytics.readonly']

    # Authenticate and create the service for the Core Reporting API
    credentials = ServiceAccountCredentials.from_json_keyfile_dict(ga_secrets, scopes)
    http_auth = credentials.authorize(Http())
    service = build('analytics', 'v3', http=http_auth)

    return service.data().ga().get(
        ids=GA_PROJECT_ID,
        start_date=dl_day,
        end_date=dl_day,
        metrics=','.join(GA_METRICS),
        dimensions='ga:date,ga:searchKeyword',
        sort='-ga:searchUniques'
    ).execute()


def create_csv(response, csv_name):
    output_file_name = f"/tmp/{csv_name}"

    with io.open(output_file_name, 'w', encoding='utf-8') as f:
        csv_wr = csv.writer(f, quoting=csv.QUOTE_ALL, dialect='excel')
        csv_wr.writerow(GA_METRICS_CSV_HEADER)

        if response.get('rows', []):
            for row in response.get('rows'):
                csv_wr.writerow(row)

    return output_file_name


def get_secret():
    session = boto3.session.Session()
    sm_client = session.client(service_name='secretsmanager', region_name=AWS_REGION)
    try:
        get_secret_value_response = sm_client.get_secret_value(SecretId=SECRET_MANAGER_NAME)
    except ClientError as e:
        raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
        else:
            secret = base64.b64decode(get_secret_value_response['SecretBinary'])

        return json.loads(secret)