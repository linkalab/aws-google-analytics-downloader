#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os, json, traceback
import boto3, s3fs
import pandas as pd
from fastparquet import write
from botocore.exceptions import ClientError
from datetime import datetime, timezone, timedelta

from src.common.shared_functions import sf_get_logger, sf_resp_payload, sf_error_paylod, sf_add_metadata_to_object

logger = sf_get_logger()
s3_client = boto3.client('s3')

S3_BUCKET = os.getenv('s3_bucket', 'ERROR_s3_bucket')
S3_INTAKE_TRANSITION_KEY = os.getenv('intake_transition_zone_s3_prefix', 'ERROR_intake_transition_zone_s3_prefix') # with trailing slash
S3_INTAKE_RAW_KEY = os.getenv('intake_raw_zone_s3_prefix', 'ERROR_intake_raw_zone_s3_prefix') # with trailing slash

def handler(event, context):
    """ Example event:
    {
        "status": "IN_TRANS_OK",
        "response": {
            "s3_key": "intake/transition/google_analytics/search_keyword/dumpdate=20201208/ga_search_keyword-20201208.csv",
            "dumpdate": "20201208"
        }
    }
    """

    logger.info("#: Event: %s" % event)
    res = event['response']
    s3_raw_key = f"{S3_INTAKE_RAW_KEY}search_keyword/dumpdate={res['dumpdate']}/ga_search_keyword-{res['dumpdate']}.parquet.gz"

    try:
        obj = s3_client.get_object(Bucket=S3_BUCKET, Key=res['s3_key'])
        df = pd.read_csv(obj['Body'])

        df['date'] = pd.to_datetime(df['date'], format='%Y%m%d')

        # Force integers for all columns
        df['uniques'] = df['uniques'].astype(int)
        df['depth'] = df['depth'].astype(int)
        df['refinements'] = df['refinements'].astype(int)
        df['duration'] = df['duration'].astype(int)
        df['exits'] = df['exits'].astype(int)

        write_df_to_parquet(df, S3_BUCKET, s3_raw_key)

        return sf_resp_payload("IN_RAW_OK", res)

    except Exception as e:
        error = traceback.format_exc()
        logger.error("Error: %s" %error, exc_info=True)
        raise e

def write_df_to_parquet(dataframe, s3_bucket, s3_key):
    """ Write a dataframe to Parquet on S3
    """
    s3 = s3fs.S3FileSystem()
    s3_key_parquet = s3_bucket+'/'+ s3_key
    write(
        filename=s3_key_parquet,
        data=dataframe,
        compression='GZIP',
        open_with=s3.open,
        times='int96'
    )

    logger.info("#: Wrote {} records to {}".format(len(dataframe), s3_key_parquet))