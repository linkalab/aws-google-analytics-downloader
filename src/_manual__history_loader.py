#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os, json, base64, urllib3, traceback, sys, logging
import time
from datetime import timedelta, date, datetime, timedelta
import boto3

boto3session = boto3.Session(profile_name='YOURAWSPROFILENAME')
sf_client = boto3session.client('stepfunctions')
STATE_MACHINE_ARN = 'arn:aws:states:eu-west-1:YOURAWSACCOUNTID:stateMachine:StateMachineGetSearchKeyword'

def main():

    start = datetime.strptime("2020-02-24", "%Y-%m-%d") # included
    end = datetime.strptime("2020-09-09", "%Y-%m-%d") # excluded
    date_generated = [start + timedelta(days=x) for x in range(0, (end-start).days)]

    for date in date_generated:
        d = date.strftime("%Y-%m-%d")
        print("Processing " + d)

        response = sf_client.start_execution(
            stateMachineArn=STATE_MACHINE_ARN,
            input=json.dumps({"date": d})
        )

        time.sleep(1)

if __name__ == "__main__":
    main()