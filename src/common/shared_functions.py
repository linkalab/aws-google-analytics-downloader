import logging
import json, os, json, traceback
import boto3
import urllib3
import re
from botocore.exceptions import ClientError
from datetime import datetime, timezone, timedelta

def sf_get_logger():
    logger = logging.getLogger()
    if logger.handlers:
        for handler in logger.handlers:
            logger.removeHandler(handler)
    logging.basicConfig(format='%(message)s', level=logging.INFO)
    logging.getLogger('boto3').setLevel(logging.ERROR)
    logging.getLogger('botocore').setLevel(logging.ERROR)
    logging.getLogger('urllib3').setLevel(logging.ERROR)
    logging.getLogger('googleapicliet.discovery_cache').setLevel(logging.ERROR)
    return logger

def sf_get_url_response(url, basic_auth):
    logger = sf_get_logger()
    logger.info("#: Calling: %s" % url)
    http = urllib3.PoolManager()
    http_headers = { "Authorization": f"Basic {basic_auth}" }
    response = http.request('GET', url, preload_content=False, headers=http_headers)
    return response

def sf_resp_payload(status, response):
    return {
        "status": status,
        "response": response,
    }

def sf_error_paylod(error, s3_bucket, s3_key):
    return {
        "error": error,
        "s3_bucket": s3_bucket,
        "s3_key":  s3_key,
    }

def sf_get_event(event):
    message = {}
    try: # SNS Message
        mess = event['Records'][0]['Sns']['Message'].strip("\n")
        message = json.loads(mess)
    except:  # SQS Message
        message = json.loads(event['Records'][0]['body'])
    return message

# https://stackoverflow.com/questions/39596987/how-to-update-metadata-of-an-existing-object-in-aws-s3-using-python-boto3
def sf_add_metadata_to_object(s3_bucket, s3_raw_key, step):
    s3_client = boto3.client('s3')
    s3_object = s3_client.get_object(Bucket=s3_bucket, Key=s3_raw_key)
    k = s3_client.head_object(Bucket=s3_bucket, Key=s3_raw_key)
    m = k["Metadata"]
    m["Step"] = "ToRaw"
    s3_client.copy_object(Bucket=s3_bucket, Key=s3_raw_key, CopySource=s3_bucket+'/'+s3_raw_key, Metadata=m, MetadataDirective='REPLACE')
    del s3_object

def sf_str4s3(str_in):
    # String compatible with S3 paths
    return re.sub('[^0-9a-zA-Z]+', '_', str_in.strip()).lower() # only alphanum and _