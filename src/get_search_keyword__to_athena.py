#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import logging

from src.common.shared_functions import sf_get_logger, sf_resp_payload, sf_error_paylod
from src.common.athena_helper import AthenaQuery

logger = sf_get_logger()

DATABASE = os.getenv('athena_db', 'ERROR_athena_db')
DB_TABLE = os.getenv('athena_ga_search_keyword_table', 'ERROR_athena_ga_search_keyword_table')
RES_BUCKET = os.getenv('athena_result_bucket', 'ERROR_athena_result_bucket')

def handler(event, context):
    """ Example event:
    {
        "status": "IN_RAW_OK",
        "response": {
            "s3_key": "intake/transition/google_analytics/search_keyword/dumpdate=20201208/ga_search_keyword-20201208.csv",
            "dumpdate": "20201208"
        }
    }
    """

    logger.info("#: Event: %s" % event)
    res = event['response']
    query = f"/*SLS_ga_search_keyword*/ ALTER TABLE {DATABASE}.{DB_TABLE} ADD IF NOT EXISTS PARTITION (dumpdate={res['dumpdate']})"
    do_query(query)

    return sf_resp_payload("ATHENA_RAW_OK", res)

def do_query(query):
    logger.info("#: Query: %s" % query)
    aq = AthenaQuery(query, DATABASE, RES_BUCKET)
    query_execution_id = aq.execute()
    result_data = aq.get_result()
    logger.info("#: query_execution_id: %s" % query_execution_id)